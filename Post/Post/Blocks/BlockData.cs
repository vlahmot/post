﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Post.Blocks
{
    class BlockData
    {
        private const float OFFSET = .125f;
        private void AddXCoord (Vector2[] vectors, float f)
        {
            for (int i = 0; i < vectors.Length; i++)
            {
                vectors[i].X += f;
            }
        }
        public Vector2[] GetTextureCoords (BlockType blockType, FaceDirection faceDir)
        {
            Vector2 topLeft = new Vector2(0.0f, 0.0f);
            Vector2 topRight = new Vector2(0.125f, 0.0f);
            Vector2 bottomLeft = new Vector2(0.0f, 0.125f);
            Vector2 bottomRight = new Vector2(0.125f, 0.125f);
            Vector2[] answer = new Vector2[4];
            switch (blockType)
            {
                case BlockType.Grass:
                    switch (faceDir)
                    {
                        case FaceDirection.XPositive:
                        case FaceDirection.XNegative:
                        case FaceDirection.ZPositive:
                        case FaceDirection.ZNegative:

                            if (faceDir == FaceDirection.XPositive)
                                return new Vector2[] { topLeft, bottomLeft, bottomRight, topRight };
                            if (faceDir == FaceDirection.XNegative)
                                return new Vector2[] { topRight, bottomLeft, bottomRight, topLeft };
                            if (faceDir == FaceDirection.ZPositive)
                                return new Vector2[] { topRight, topLeft, bottomRight, bottomLeft };
                            if (faceDir == FaceDirection.ZNegative)
                                return new Vector2[] { topLeft, bottomLeft, topRight, bottomRight };
                            break;

                        case FaceDirection.YNegative:
                            Vector2[] newVectors = new Vector2[]
                                { topLeft, bottomLeft, bottomRight,topRight };
                            AddXCoord(newVectors, OFFSET);
                            answer = newVectors;
                            break;

                        case FaceDirection.YPositive:
                            Vector2[] newVectors2 = new Vector2[]
                                { bottomLeft,topRight,topLeft,bottomRight};
                            AddXCoord(newVectors2, OFFSET * 2.0f);
                            answer = newVectors2;
                            break;
                        //default:
                            //break;
                    }

                    break;


                //default:
                    //break;
            }


            return answer;

        }
    }


}
