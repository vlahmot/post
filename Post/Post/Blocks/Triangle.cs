﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Post.Blocks
{
    class Triangle
    {
        private int[] indices;

        public Triangle(Vector3 triangleVertedIndices)
        {
            indices = new int[3];
            indices[0] = (int)triangleVertedIndices.X;
            indices[1] = (int)triangleVertedIndices.Y;
            indices[2] = (int)triangleVertedIndices.Z;
        }

        public int[] Indices
        {
            get { return indices; }
        }
    }
}
