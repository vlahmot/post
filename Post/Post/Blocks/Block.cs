﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Post.Blocks
{
    public enum BlockType
    {
        None = 0,
        Grass = 1
    };

    public enum FaceDirection
    { 
        XPositive,
        XNegative,
        YPositive,
        YNegative,
        ZPositive,
        ZNegative

    };

    class Block
    {

        private BlockType blockType;

        public BlockType BlockType
        {
            get { return blockType; }
        }

        public Block(BlockType type)
        {
            blockType = type;


        }


    }
}
