﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Post.Blocks;

namespace Post.World
{
    class WorldModel
    {
        Game1 game;
        private const int BLOCKSIZE = 1;
        private int faceCounter = 0;
        
        private Vector3 worldSize;
        BlockData blockData = new BlockData();

        Chunk[,,] chunkArray;

        

        public Game1 Game
        {
            get { return game; }
        }
        public Vector3 WorldSize
        {
            get { return worldSize; }
        }
        public int FaceCounter
        {
            get { return faceCounter; }
        }
       

        public WorldModel (Vector3 size, Game1 game1)
        {
            worldSize = size;
            int x = (int)worldSize.X;
            int y = (int)worldSize.Y;

            int z = (int)worldSize.Z; 
            game = game1;
            chunkArray = new Chunk[x,y,z];
            InitializeChunks(worldSize, game1);
         

          
        }

        private void InitializeChunks (Vector3 worldSize, Game1 game1)
        {
            int x = (int)worldSize.X;
            int y = (int)worldSize.Y;

            int z = (int)worldSize.Z; 
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    for (int k = 0; k < z; k++)
                    {
                        chunkArray[i, j, k] = new Chunk(new Vector3(i, j, k), this);
                    }
                }
            }
        }

        public void DrawWorld (GraphicsDevice device)
        {

            for (int i = 0; i < chunkArray.GetLength(0); i++)
            {
                for (int j = 0; j < chunkArray.GetLength(1); j++)
                {
                    for (int k = 0; k < chunkArray.GetLength(2); k++)
                    {
                        device.SetVertexBuffer(chunkArray[i,j,k].ChunkVertBuffer);
                        device.Indices = chunkArray[i, j, k].ChunkIndexBuffer;

                        device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, chunkArray[i, j, k].ChunkVertBuffer.VertexCount, 0, chunkArray[i, j, k].FaceCount * 2);
                    }
                }
            }
            /*
            foreach (Chunk chunk in chunkArray)
            {
                device.SetVertexBuffer(chunk.ChunkVertBuffer);
                device.Indices = chunk.ChunkIndexBuffer;

                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,chunk.ChunkVertBuffer.VertexCount, 0, chunk.FaceCount * 2);
            }
             */
        }
        




    }
}
