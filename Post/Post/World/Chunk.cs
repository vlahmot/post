﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Post.Blocks;

namespace Post.World
{
    class Chunk
    {
        private const int BLOCKSIZE = 1;
        private Vector3 CHUNKSIZE = new Vector3(32, 32, 32);

        private WorldModel world;

        private Vector3 chunkPosition;
        private int faceCount;
        private VertexBuffer vertBuffer;
        private IndexBuffer indexBuffer;
        private Block[, ,] blockArray;

        BlockData blockData = new BlockData();


        private List<VertexPositionColorTexture> vertexList;
        private List<int> indicesList;

        public int FaceCount
        {
            get { return faceCount;}
        }

        public VertexBuffer ChunkVertBuffer
        {
            get { return vertBuffer; }

        }

        public IndexBuffer ChunkIndexBuffer
        {
            get { return indexBuffer; }
        }

        public Block[, ,] BlockArray
        {
            get { return blockArray; }
        }

        public Chunk (Vector3 position, WorldModel worldModel)
        {
            faceCount = 0;
            world = worldModel;
            chunkPosition = position;
            vertexList = new List<VertexPositionColorTexture>();
            indicesList = new List<int>();

            InitializeBlockArray();

            SetUpVisibleVertexIndex();

            vertBuffer = new VertexBuffer(world.Game.GraphicsDevice, VertexPositionColorTexture.VertexDeclaration,vertexList.Count, BufferUsage.WriteOnly);
            indexBuffer = new IndexBuffer(world.Game.GraphicsDevice, IndexElementSize.ThirtyTwoBits,indicesList.Count, BufferUsage.WriteOnly);
            vertBuffer.SetData<VertexPositionColorTexture>(vertexList.ToArray());
            indexBuffer.SetData(indicesList.ToArray());

            
        }

        private void SetUpVisibleVertexIndex ()
        {
            int vertCounter = 0;
            int indCounter = 0;

            for (int i = 0; i < blockArray.GetLength(0); i++)
            {
                for (int j = 0; j < blockArray.GetLength(1); j++)
                {
                    for (int k = 0; k < blockArray.GetLength(2); k++)
                    {
                        foreach (var item in Enum.GetValues(typeof(FaceDirection)))
                        {
                            int[] indexBuffer;
                            VertexPositionColorTexture[] vertBuffer = GetVisibleFaceVertices(new Vector3(i, j, k), (FaceDirection)item);
                            if (vertBuffer[0].Color.R != 0)  //face needs to be drawn. Checks for non-default value (ie whether a vertex is there or the array is "empty")
                            {
                                indexBuffer = GetFaceIndices((FaceDirection)item);
                                faceCount++;
                                for (int x = 0; x < indexBuffer.Length; x++)
                                {

                                    indicesList.Add(indexBuffer[x] + vertCounter);
                                }
                                for (int y = 0; y < vertBuffer.Length; y++)
                                {
                                    vertexList.Add(vertBuffer[y]);
                                }
                                vertCounter += 4;
                                indCounter += 6;
                            }


                        }
                    }
                }
            }
        }


        private VertexPositionColorTexture[] GetVisibleFaceVertices (Vector3 bottomLeftCornerVertex, FaceDirection faceDir)
        {
            float x = bottomLeftCornerVertex.X;
            float y = bottomLeftCornerVertex.Y;
            float z = bottomLeftCornerVertex.Z;

            int i = (int)x;
            int j = (int)y;
            int k = (int)z;

            int offsetX = 8 * (int)chunkPosition.X;
            int offsetY = 8 * (int)chunkPosition.Y;
            int offsetZ = 8 * (int)chunkPosition.Z;


            Vector3 topLeftCornerFrontVertex = new Vector3(x + offsetX, y + offsetY + BLOCKSIZE, z + offsetZ - BLOCKSIZE);
            Vector3 bottomLeftCornerFrontVertex = new Vector3(x + offsetX, y + offsetY, z + offsetZ - BLOCKSIZE);
            Vector3 topRightCornerFrontVertex = new Vector3(x + offsetX + BLOCKSIZE, y + offsetY + BLOCKSIZE, z + offsetZ - BLOCKSIZE);
            Vector3 bottomRightCornerFrontVertex = new Vector3(x + offsetX + BLOCKSIZE, y + offsetY, z + offsetZ - BLOCKSIZE);

            Vector3 topLeftCornerBackVertex = new Vector3(x + offsetX, y + offsetY + BLOCKSIZE, z + offsetZ);
            Vector3 topRightCornerBackVertex = new Vector3(x + offsetX + BLOCKSIZE, y + offsetY + BLOCKSIZE, z + offsetZ);
            Vector3 bottomLeftCornerBackVertex = bottomLeftCornerVertex + new Vector3(offsetX,offsetY,offsetZ);
            Vector3 bottomRightCornerBackVertex = new Vector3(x + offsetX + BLOCKSIZE, y + offsetY, z + offsetZ);

            Vector2 topLeftUV = new Vector2(0.0f, 0.0f);
            Vector2 topRightUV = new Vector2(0.125f, 0.0f);
            Vector2 bottomLeftUV = new Vector2(0.0f, 0.125f);
            Vector2 bottomRightUV = new Vector2(0.125f, 0.125f);

            VertexPositionColorTexture[] vertices = new VertexPositionColorTexture[4];

            BlockType blockType = blockArray[(int)bottomLeftCornerVertex.X, (int)bottomLeftCornerVertex.Y,
                        (int)bottomLeftCornerVertex.Z].BlockType;
            Vector2[] texCoords;
            switch (faceDir)
            {
                case FaceDirection.XPositive:

                    texCoords = blockData.GetTextureCoords(blockType, FaceDirection.XPositive);
                    if (i == blockArray.GetLength(0) - 1)
                    {
                        vertices[0] = new VertexPositionColorTexture(topRightCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomRightCornerFrontVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomRightCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(topRightCornerBackVertex, Color.White, texCoords[3]);
                        break;
                    }
                    Block back = blockArray[i + 1, j, k];   // +x
                    if (back.BlockType == BlockType.None)
                    {

                        vertices[0] = new VertexPositionColorTexture(topRightCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomRightCornerFrontVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomRightCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(topRightCornerBackVertex, Color.White, texCoords[3]);
                    }
                    break;
                case FaceDirection.XNegative:

                    texCoords = blockData.GetTextureCoords(blockType, FaceDirection.XNegative);

                    if (i == 0)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomLeftCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomLeftCornerFrontVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(topLeftCornerBackVertex, Color.White, texCoords[3]);
                        break;
                    }
                    Block front = blockArray[i - 1, j, k];      // -x
                    if (front.BlockType == BlockType.None)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomLeftCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomLeftCornerFrontVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(topLeftCornerBackVertex, Color.White, texCoords[3]);
                    }
                    break;
                case FaceDirection.YPositive:
                    texCoords = blockData.GetTextureCoords(blockType, FaceDirection.YPositive);

                    if (j == blockArray.GetLength(1) - 1)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(topRightCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(topLeftCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(topRightCornerFrontVertex, Color.White, texCoords[3]);
                        break;
                    }
                    Block above = blockArray[i, j + 1, k];      // +y
                    if (above.BlockType == BlockType.None)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(topRightCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(topLeftCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(topRightCornerFrontVertex, Color.White, texCoords[3]);
                    }
                    break;
                case FaceDirection.YNegative:
                    texCoords = blockData.GetTextureCoords(blockType, FaceDirection.YNegative);
                    if (j == 0)
                    {
                        vertices[0] = new VertexPositionColorTexture(bottomLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomLeftCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomRightCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(bottomRightCornerFrontVertex, Color.White, texCoords[3]);
                        break;
                    }
                    Block below = blockArray[i, j - 1, k];      // -y
                    if (below.BlockType == BlockType.None)
                    {
                        vertices[0] = new VertexPositionColorTexture(bottomLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomLeftCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomRightCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(bottomRightCornerFrontVertex, Color.White, texCoords[3]);
                    }
                    break;
                case FaceDirection.ZPositive:
                    texCoords = blockData.GetTextureCoords(blockType, FaceDirection.ZPositive);
                    if (k == blockArray.GetLength(2) - 1)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerBackVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(topRightCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomLeftCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(bottomRightCornerBackVertex, Color.White, texCoords[3]);
                        break;
                    }
                    Block right = blockArray[i, j, k + 1];      // +z
                    if (right.BlockType == BlockType.None)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerBackVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(topRightCornerBackVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(bottomLeftCornerBackVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(bottomRightCornerBackVertex, Color.White, texCoords[3]);
                    }
                    break;
                case FaceDirection.ZNegative:
                    texCoords = blockData.GetTextureCoords(blockType, FaceDirection.ZNegative);
                    if (k == 0)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomLeftCornerFrontVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(topRightCornerFrontVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(bottomRightCornerFrontVertex, Color.White, texCoords[3]);
                        break;
                    }
                    Block left = blockArray[i, j, k - 1];       // -z
                    if (left.BlockType == BlockType.None)
                    {
                        vertices[0] = new VertexPositionColorTexture(topLeftCornerFrontVertex, Color.White, texCoords[0]);
                        vertices[1] = new VertexPositionColorTexture(bottomLeftCornerFrontVertex, Color.White, texCoords[1]);
                        vertices[2] = new VertexPositionColorTexture(topRightCornerFrontVertex, Color.White, texCoords[2]);
                        vertices[3] = new VertexPositionColorTexture(bottomRightCornerFrontVertex, Color.White, texCoords[3]);
                    }
                    break;

                default:
                    break;

            }

            return vertices;

        }



        private int[] GetFaceIndices (FaceDirection faceDir)
        {
            int[] indices = new int[6];
            switch (faceDir)
            {

                case FaceDirection.XPositive:

                    indices = new int[] { 0, 1, 2, 3, 0, 2 };
                    break;
                case FaceDirection.XNegative:
                    indices = new int[] { 0, 1, 2, 3, 1, 0 };
                    break;
                case FaceDirection.YPositive:
                    indices = new int[] { 0, 1, 2, 0, 3, 1 };
                    break;
                case FaceDirection.YNegative:
                    indices = new int[] { 0, 1, 2, 0, 2, 3 };
                    break;
                case FaceDirection.ZPositive:
                    indices = new int[] { 0, 1, 2, 2, 1, 3 };
                    break;
                case FaceDirection.ZNegative:
                    indices = new int[] { 0, 1, 2, 1, 3, 2 };
                    break;
                default:
                    break;
            }
            return indices;

        }

        private void InitializeBlockArray ()
        {

            blockArray = new Block[(int)CHUNKSIZE.X, (int)CHUNKSIZE.Y, (int)CHUNKSIZE.Z];

            for (int i = 0; i < blockArray.GetLength(0); i++)
            {
                for (int j = 0; j < blockArray.GetLength(1); j++)
                {
                    for (int k = 0; k < blockArray.GetLength(2); k++)
                    {
                        blockArray[i, j, k] = new Block(BlockType.Grass);
                    }
                }
            }
        }



    }
}
