using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;

using Post.Blocks;
using Post.World;

namespace Post
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        GraphicsDevice device;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;
        Effect effect;
       


        WorldModel world;

        RasterizerState rs;
        DepthStencilState depthState;

        FpsCounter fpsC = new FpsCounter();
        


        QuakeCamera cam;

        Texture2D blockTexture;
        Texture2D blockTexture2;


        CoordCross coordCross;

      
        

        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
           
            Content.RootDirectory = "Content";
        }

     

        
      

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
          
            graphics.IsFullScreen = false;
            //Window.AllowUserResizing = true;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();
            cam = new QuakeCamera(this.GraphicsDevice.Viewport);
            
        

            world = new WorldModel(new Vector3(1, 1,1),this);
           



            Debug.isDrawn = true;


            Window.Title = "Post";

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>(@"Content\Fonts\Pericles");
            device = graphics.GraphicsDevice;
            
            /*
            SamplerState ss = new SamplerState();
            ss.Filter = TextureFilter.LinearMipPoint;
            ss.AddressU = TextureAddressMode.Clamp;
            ss.AddressV = TextureAddressMode.Clamp;
            ss.AddressW = TextureAddressMode.Clamp;
            device.SamplerStates[0] = ss;
             */
            
            blockTexture = Content.Load<Texture2D>(@"Content\BlockTexture");
            blockTexture2 = Content.Load<Texture2D>(@"Content\BlockTexture2");
            effect = Content.Load<Effect>(@"Content\effects");
            coordCross = new CoordCross(device);
        
            /*
            vertBuffer = new VertexBuffer(device, VertexPositionColorTexture.VertexDeclaration, world.Vertices.Length, BufferUsage.WriteOnly);
            indexBuffer = new IndexBuffer(device,IndexElementSize.ThirtyTwoBits, world.Indices.Length, BufferUsage.WriteOnly);

            vertBuffer.SetData(world.Vertices);
            indexBuffer.SetData(world.Indices);

             */
             rs = new RasterizerState();
            rs.CullMode = CullMode.CullCounterClockwiseFace;
            //rs.FillMode = FillMode.WireFrame;
            device.RasterizerState = rs;
            depthState = new DepthStencilState();
            depthState.DepthBufferEnable = true;
            depthState.DepthBufferWriteEnable = true;
            GraphicsDevice.DepthStencilState = depthState;

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            //fpsC.Update(gameTime);
            cam.Update(Mouse.GetState(), Keyboard.GetState(), GamePad.GetState(0));


            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            
            fpsC.Update(gameTime);
            device.DepthStencilState = depthState;
            device.RasterizerState = rs;
            
            
            device.Clear(Color.CornflowerBlue);
            effect.CurrentTechnique = effect.Techniques["ColoredNoShading"];

            Matrix worldMatrix = Matrix.Identity;

            effect.Parameters["xView"].SetValue(cam.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cam.ProjectionMatrix);
            effect.Parameters["xWorld"].SetValue(worldMatrix);
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
            }
            coordCross.DrawUsingPresetEffect();

            effect.CurrentTechnique = effect.Techniques["TexturedNoShading"];
           
             
            effect.Parameters["xWorld"].SetValue(worldMatrix);
            effect.Parameters["xTexture"].SetValue(blockTexture2);

            effect.Parameters["xTexture"].SetValue(blockTexture);
            
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
            }
        
          
          
            /*
            device.SetVertexBuffer(vertBuffer);
            device.Indices = indexBuffer;
          
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,world.Vertices.Length, 0,world.FaceCounter * 2);*/
            world.DrawWorld(device);
          
            spriteBatch.Begin();
           
            Debug.drawText(spriteBatch, spriteFont, "FPS: " + fpsC.GetRate(), new Vector2(0, 10));
            Debug.drawText(spriteBatch, spriteFont, "Faces Drawn: " + world.FaceCounter, new Vector2(0, 20));
            Debug.drawText(spriteBatch, spriteFont, "Actual Faces: " + world.WorldSize.X * world.WorldSize.Y * world.WorldSize.Z * 3072, new Vector2(0, 30));
            //Debug.drawText(spriteBatch, spriteFont, "Drawn Vertices: " + world.Vertices.Length, new Vector2(0, 40));
            Debug.drawText(spriteBatch, spriteFont, "Actual Vertices: " + world.WorldSize.X * world.WorldSize.Y * world.WorldSize.Z * 12288, new Vector2(0, 50));
            //Debug.drawText(spriteBatch, spriteFont, "Used Indices: " + world.Indices.Length, new Vector2(0, 60));
            Debug.drawText(spriteBatch, spriteFont, "Actual Indices: " + world.WorldSize.X * world.WorldSize.Y * world.WorldSize.Z * 18432, new Vector2(0, 70));
            spriteBatch.End();
          
                              
             
         
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
