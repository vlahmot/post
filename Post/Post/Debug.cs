﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Post
{
    static class Debug
    {
        private static bool _isDrawn;

        public static bool isDrawn
        {
            get { return _isDrawn; }
            set { _isDrawn = value; }
        }

        /// <summary>
        /// Verifies that debug text is being draw, then draws the text.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch used to draw the text.</param>
        /// <param name="spriteFont">The font used to draw the text.</param>
        /// <param name="text">The text of the debug message.</param>
        /// <param name="position">The position on screen for the text.</param>
        public static void drawText(SpriteBatch spriteBatch, SpriteFont spriteFont, string text, Vector2 position)
        {
            if (_isDrawn)
            {
                spriteBatch.DrawString(spriteFont, text, position, Color.White, 0.0f, new Vector2(0, 0), .5f, SpriteEffects.None, 1.0f);
            }
        }
    }

    /// <summary>
    /// The fpsCounter class keeps track of how many frames are being displayed each second
    /// </summary>
    class FpsCounter
    {
        private int frameRate = 0;
        private int frameCounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;

        /// <summary>
        /// Adds the amount of elapsed time since the last update to the time counter
        /// If 1 second has passed reset the time, set the framerate equal to the current frameCounter
        /// Reset the frameCounter
        /// This allows the frameRate variable to hold the count of frames in one second
        /// </summary>
        /// <param name="gametime">The current gametime</param>
        public void Update(GameTime gametime)
        {
            IncrimentFrameCounter();
            elapsedTime += gametime.ElapsedGameTime;
            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
        }

        public void IncrimentFrameCounter()
        {
            frameCounter++;
        }

        public string GetRate()
        {
            return frameRate.ToString();
        }
    }
}